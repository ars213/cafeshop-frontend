import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [
  {
    path : '',
    pathMatch : 'full',
    component : DashboardComponent
  },
  {
    path : 'Dashboard',
    pathMatch : 'full',
    component : DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    useHash : true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
